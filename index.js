"use strict";

var fs = require("fs");
var _ = require("underscore");
var moment = require("moment");

var runner = require("./lib/caseRunner.js");
var reportCompiler = require("./lib/reportCompiler.js");
var reportServer = require("./lib/reportServer");
var reportWriter = require("./lib/reportWriter.js");
var reportConf = require("./lib/reportConf.js");


function runTest(caseList, testConf){
    // 1. Inherit default configuration.
    if(!testConf) testConf = {};
    _.extend(testConf, reportConf.defaultConf, testConf);

    // 2. Run test cases.
    runner.run(caseList, testConf).def.then(function(caseList){
        console.log("Test Over: ", "All cases executed.");

        reportCompiler.compileSummaryReport(caseList, testConf);

        reportWriter.generateReports(caseList, testConf);

        reportServer.startServer(testConf);

    });
}

exports.runTest = runTest;



