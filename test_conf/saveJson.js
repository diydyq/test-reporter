"use strict";

/**
 * 读取caseList, 执行测试，保存到JSON文件
 */

var fs = require("fs");
var _ = require("underscore");
var moment = require("moment");

var runner = require("../lib/caseRunner.js");
var reportConf = require("../lib/reportConf.js");


function runTest(caseList, testConf){
    // 1. Inherit default configuration.
    if(!testConf) testConf = {};
    _.extend(testConf, reportConf.defaultConf, testConf);

    // 2. Run test cases.
    runner.run(caseList, testConf).def.then(function(caseList){
        console.log("Test Over: ", "All cases executed.");

        fs.writeFileSync("test/caseList.json", JSON.stringify(caseList, null, 4));
        fs.writeFileSync("test/testConf.json", JSON.stringify(testConf, null, 4));
        return;
    });
}

exports.runTest = runTest;



