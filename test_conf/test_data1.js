var testConf = {
	browsers: ["chrome"],
	protocal: "http",
	host: "localhost",
	port: null,
	caseTimeout: 10*1000,
	testLib: {
		name: "mocha",
		fnWaitFlag: waitOver,
		fnWaitReturn: waitResult,
		fnPageSource: pageSource
	},

	summary: {
		fileName: "report_summary.html",
		fileHtml: "Test",

		timeStart: new Date,
		timeEnd: new Date
	},
	serverConf: {
		root: ".tmpServer",
		port: 8000,
		host: "0.0.0.0",
		// Server context path.
		path: "C:\\Users\\yongqingdong\\AppData\\Local/.tmpServer/"
	}
};


var caseList = [];
caseList.push({
	name: "",
	modName: "lib:unittest/cases/mocha/case.vm",
	path: "/lib/unittest/cases/mocha/case.vm",
	url: "/lib/unittest/cases/mocha/case.vm",
	result: {
		page: {
			name: "lib.unittest.cases.mocha.case.vm",
			html: "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>    <meta charset=\"utf-8\" />\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>\n    <title>Mocha Example</title>\n    \n            \n                \n                \n        <link href=\"http://cdn.bootcss.com/mocha/2.0.1/mocha.css\" rel=\"stylesheet\"><link rel=\"stylesheet\" type=\"text/css\" href=\"/static/lib/css/bootstrap/bootstrap.css\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"/static/lib/css/bootstrap/todc-bootstrap.css\" /></head>\n<body>    <div id=\"mocha\">\n    <ul id=\"mocha-stats\"><li class=\"progress\"><canvas width=\"40\" height=\"40\"></canvas></li><li class=\"passes\"><a href=\"#\">passes:</a> <em>4</em></li><li class=\"failures\"><ahref=\"#\">failures:</a> <em>1</em></li><li class=\"duration\">duration: <em>1.09</em>s</li></ul><ul id=\"mocha-report\"><li class=\"suite\"><h1><a href=\"?grep=Array\">Array</a></h1><ul><li class=\"suite\"><h1><a href=\"?grep=Array%201.%20%23indexOf()\">1. #indexOf()</a></h1><ul><li class=\"test pass fast\"><h2>1. should return -1 when the value is not present<span class=\"duration\">2ms</span> <a href=\"?grep=Array%201.%20%23indexOf()%201.%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));\n[<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>).should.equal(-<span class=\"number\">1</span>);</code></pre></li><li class=\"test fail\"><h2>2. should return -1 when the value is not present <a href=\"?grep=Array%201.%20%23indexOf()%202.%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre class=\"error\">AssertionError: expected 0 to equal -1\n  at Context.&lt;anonymous&gt; (http://localhost/static/lib/unittest/cases/mocha/case.js:17:32)\n    at callFn (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4428:21)\n    at Test.Runnable.run (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4421:7)\n    at Runner.runTest (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4823:10)\n  at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4901:12\n    at next (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4748:14)\n    at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4758:7\n    at next (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4696:23)\n    at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4725:5\n    at timeslice (http://localhost/static/lib/unittest/lib/mocha/mocha.js:5989:27)</pre><pre style=\"display:none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(<span class=\"number\">0</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass pending\"><h2>3. should return -1 when the value is not present</h2></li><li class=\"test pass pending\"><h2>4. should return -1 when the value is not present</h2></li></ul></li><li class=\"suite\"><h1><a href=\"?grep=Array%202.%20%23indexOf()\">2. #indexOf()</a></h1><ul><li class=\"test pass fast\"><h2>should return -1 when the value is not present<span class=\"duration\">0ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass fast\"><h2>should return -1 when the value is not present<span class=\"duration\">0ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass slow\"><h2>should return -1 when the value is not present<span class=\"duration\">1001ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code><span class=\"comment\">// Expect done was called <span class=\"keyword\">if</span> \"done\" argument existed.</span>\nsetTimeout(done, <span class=\"number\">1000</span>);</code></pre></li></ul></li><li class=\"suite\"><h1><a href=\"?grep=Array%203.%20%23indexOf()\">3. #indexOf()</a></h1><ul><li class=\"test pass pending\"><h2>should return -1 when the value is not present</h2></li><li class=\"test pass pending\"><h2>should return -1 when the value is not present</h2></li></ul></li></ul></li></ul></div>\n\n    \n    \n<script type=\"text/javascript\" src=\"/static/lib/js/mod/mod.js\"></script><script type=\"text/javascript\" src=\"/static/lib/js/jquery/jquery-1.11.1.js\"></script><script type=\"text/javascript\" src=\"/static/lib/js/jquery/jquery-migrate-1.2.1.js\"></script><script type=\"text/javascript\" src=\"/static/lib/js/bootstrap/bootstrap.js\"></script><script type=\"text/javascript\" src=\"/static/lib/unittest/lib/mocha/mocha.js\"></script><script type=\"text/javascript\" src=\"/static/lib/unittest/lib/mocha/mochaRunnerWrap.js\"></script><script type=\"text/javascript\" src=\"/static/lib/unittest/lib/mocha/chai.js\"></script><script type=\"text/javascript\" src=\"/static/lib/unittest/cases/mocha/case.js\"></script><script type=\"text/javascript\">    var assert = chai.assert;\n    var expect = chai.expect;\n    var should = chai.should();\n    mocha.setup({\n        ui: \"bdd\"\n    });\n\n\n\n        var spec = require(\"lib:unittest/cases/mocha/case.js\")\n    spec.init();\n\n    mocha.run();\n</script> \n \n\n\n</body></html>",
			pure: "<div id=\"mocha\">\n    <ul id=\"mocha-stats\"><li class=\"progress\"><canvas width=\"40\" height=\"40\"></canvas></li><li class=\"passes\"><a href=\"#\">passes:</a> <em>4</em></li><li class=\"failures\"><ahref=\"#\">failures:</a> <em>1</em></li><li class=\"duration\">duration: <em>1.09</em>s</li></ul><ul id=\"mocha-report\"><li class=\"suite\"><h1><a href=\"?grep=Array\">Array</a></h1><ul><li class=\"suite\"><h1><a href=\"?grep=Array%201.%20%23indexOf()\">1. #indexOf()</a></h1><ul><li class=\"test pass fast\"><h2>1. should return -1 when the value is not present<span class=\"duration\">2ms</span> <a href=\"?grep=Array%201.%20%23indexOf()%201.%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));\n[<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>).should.equal(-<span class=\"number\">1</span>);</code></pre></li><li class=\"test fail\"><h2>2. should return -1 when the value is not present <a href=\"?grep=Array%201.%20%23indexOf()%202.%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre class=\"error\">AssertionError: expected 0 to equal -1\n  at Context.&lt;anonymous&gt; (http://localhost/static/lib/unittest/cases/mocha/case.js:17:32)\n    at callFn (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4428:21)\n    at Test.Runnable.run (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4421:7)\n    at Runner.runTest (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4823:10)\n  at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4901:12\n    at next (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4748:14)\n    at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4758:7\n    at next (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4696:23)\n    at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4725:5\n    at timeslice (http://localhost/static/lib/unittest/lib/mocha/mocha.js:5989:27)</pre><pre style=\"display:none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(<span class=\"number\">0</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass pending\"><h2>3. should return -1 when the value is not present</h2></li><li class=\"test pass pending\"><h2>4. should return -1 when the value is not present</h2></li></ul></li><li class=\"suite\"><h1><a href=\"?grep=Array%202.%20%23indexOf()\">2. #indexOf()</a></h1><ul><li class=\"test pass fast\"><h2>should return -1 when the value is not present<span class=\"duration\">0ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass fast\"><h2>should return -1 when the value is not present<span class=\"duration\">0ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass slow\"><h2>should return -1 when the value is not present<span class=\"duration\">1001ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code><span class=\"comment\">// Expect done was called <span class=\"keyword\">if</span> \"done\" argument existed.</span>\nsetTimeout(done, <span class=\"number\">1000</span>);</code></pre></li></ul></li><li class=\"suite\"><h1><a href=\"?grep=Array%203.%20%23indexOf()\">3. #indexOf()</a></h1><ul><li class=\"test pass pending\"><h2>should return -1 when the value is not present</h2></li><li class=\"test pass pending\"><h2>should return -1 when the value is not present</h2></li></ul></li></ul></li></ul></div>",
			shot: "DDDDDDDD"
		},
		json: {
		  "stats": {
		    "suites": 4,
		    "tests": 9,
		    "passes": 4,
		    "pending": 4,
		    "failures": 1,
		    "start": "2014-11-21T06:39:07.853Z",
		    "end": "2014-11-21T06:39:09.077Z",
		    "duration": 1224
		  },
		  "tests": [
		    {
		      "title": "1. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 1. should return -1 when the value is not present",
		      "duration": 1,
		      "err": {}
		    },
		    {
		      "title": "2. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 2. should return -1 when the value is not present",
		      "duration": 2,
		      "err": {
		        "message": "expected 0 to equal -1",
		        "showDiff": false,
		        "actual": 0,
		        "expected": -1,
		        "stack": "AssertionError: expected 0 to equal -1\n    at Context.<anonymous> (http://nuomi.com/static/lib/unittest/cases/mocha/case.js:17:32)\n    at callFn (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4428:21)\n    at Test.Runnable.run (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4421:7)\n    at Runner.runTest (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4823:10)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4901:12\n    at next (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4748:14)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4758:7\n    at next (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4696:23)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4725:5\n    at timeslice (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:5989:27)"
		      }
		    },
		    {
		      "title": "3. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 3. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "4. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 4. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 1001,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    }
		  ],
		  "pending": [
		    {
		      "title": "3. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 3. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "4. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 4. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    }
		  ],
		  "failures": [
		    {
		      "title": "2. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 2. should return -1 when the value is not present",
		      "duration": 2,
		      "err": {
		        "message": "expected 0 to equal -1",
		        "showDiff": false,
		        "actual": 0,
		        "expected": -1,
		        "stack": "AssertionError: expected 0 to equal -1\n    at Context.<anonymous> (http://nuomi.com/static/lib/unittest/cases/mocha/case.js:17:32)\n    at callFn (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4428:21)\n    at Test.Runnable.run (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4421:7)\n    at Runner.runTest (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4823:10)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4901:12\n    at next (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4748:14)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4758:7\n    at next (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4696:23)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4725:5\n    at timeslice (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:5989:27)"
		      }
		    }
		  ],
		  "passes": [
		    {
		      "title": "1. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 1. should return -1 when the value is not present",
		      "duration": 1,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 1001,
		      "err": {}
		    }
		  ]
		}
	}	
});
caseList.push({
	name: "",
	modName: "common:unittest/cases/TreeNodesSelect/test.vm",
	path: "/common/unittest/cases/TreeNodesSelect/test.vm",
	url: "/common/unittest/cases/TreeNodesSelect/test.vm",
	result: {
		page: {
			name: "lib.unittest.cases.TreeNodesSelect.test.vm",
			html: "",
			pure: "<div id=\"mocha\">\n    <ul id=\"mocha-stats\"><li class=\"progress\"><canvas width=\"40\" height=\"40\"></canvas></li><li class=\"passes\"><a href=\"#\">passes:</a> <em>4</em></li><li class=\"failures\"><ahref=\"#\">failures:</a> <em>1</em></li><li class=\"duration\">duration: <em>1.09</em>s</li></ul><ul id=\"mocha-report\"><li class=\"suite\"><h1><a href=\"?grep=Array\">Array</a></h1><ul><li class=\"suite\"><h1><a href=\"?grep=Array%201.%20%23indexOf()\">1. #indexOf()</a></h1><ul><li class=\"test pass fast\"><h2>1. should return -1 when the value is not present<span class=\"duration\">2ms</span> <a href=\"?grep=Array%201.%20%23indexOf()%201.%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));\n[<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>).should.equal(-<span class=\"number\">1</span>);</code></pre></li><li class=\"test fail\"><h2>2. should return -1 when the value is not present <a href=\"?grep=Array%201.%20%23indexOf()%202.%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre class=\"error\">AssertionError: expected 0 to equal -1\n  at Context.&lt;anonymous&gt; (http://localhost/static/lib/unittest/cases/mocha/case.js:17:32)\n    at callFn (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4428:21)\n    at Test.Runnable.run (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4421:7)\n    at Runner.runTest (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4823:10)\n  at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4901:12\n    at next (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4748:14)\n    at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4758:7\n    at next (http://localhost/static/lib/unittest/lib/mocha/mocha.js:4696:23)\n    at http://localhost/static/lib/unittest/lib/mocha/mocha.js:4725:5\n    at timeslice (http://localhost/static/lib/unittest/lib/mocha/mocha.js:5989:27)</pre><pre style=\"display:none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(<span class=\"number\">0</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass pending\"><h2>3. should return -1 when the value is not present</h2></li><li class=\"test pass pending\"><h2>4. should return -1 when the value is not present</h2></li></ul></li><li class=\"suite\"><h1><a href=\"?grep=Array%202.%20%23indexOf()\">2. #indexOf()</a></h1><ul><li class=\"test pass fast\"><h2>should return -1 when the value is not present<span class=\"duration\">0ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass fast\"><h2>should return -1 when the value is not present<span class=\"duration\">0ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code>assert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">5</span>));\nassert.equal(-<span class=\"number\">1</span>, [<span class=\"number\">1</span>,<span class=\"number\">2</span>,<span class=\"number\">3</span>].indexOf(<span class=\"number\">0</span>));</code></pre></li><li class=\"test pass slow\"><h2>should return -1 when the value is not present<span class=\"duration\">1001ms</span> <a href=\"?grep=Array%202.%20%23indexOf()%20should%20return%20-1%20when%20the%20value%20is%20not%20present\" class=\"replay\">‣</a></h2><pre style=\"display: none;\"><code><span class=\"comment\">// Expect done was called <span class=\"keyword\">if</span> \"done\" argument existed.</span>\nsetTimeout(done, <span class=\"number\">1000</span>);</code></pre></li></ul></li><li class=\"suite\"><h1><a href=\"?grep=Array%203.%20%23indexOf()\">3. #indexOf()</a></h1><ul><li class=\"test pass pending\"><h2>should return -1 when the value is not present</h2></li><li class=\"test pass pending\"><h2>should return -1 when the value is not present</h2></li></ul></li></ul></li></ul></div>",
			shot: "DDDDDDDDDD"
		},
		json: {
		  "stats": {
		    "suites": 4,
		    "tests": 9,
		    "passes": 4,
		    "pending": 4,
		    "failures": 0,
		    "start": "2014-11-21T06:39:07.853Z",
		    "end": "2014-11-21T06:39:09.077Z",
		    "duration": 1224
		  },
		  "tests": [
		    {
		      "title": "1. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 1. should return -1 when the value is not present",
		      "duration": 1,
		      "err": {}
		    },
		    {
		      "title": "2. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 2. should return -1 when the value is not present",
		      "duration": 2,
		      "err": {
		        "message": "expected 0 to equal -1",
		        "showDiff": false,
		        "actual": 0,
		        "expected": -1,
		        "stack": "AssertionError: expected 0 to equal -1\n    at Context.<anonymous> (http://nuomi.com/static/lib/unittest/cases/mocha/case.js:17:32)\n    at callFn (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4428:21)\n    at Test.Runnable.run (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4421:7)\n    at Runner.runTest (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4823:10)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4901:12\n    at next (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4748:14)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4758:7\n    at next (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4696:23)\n    at http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:4725:5\n    at timeslice (http://nuomi.com/static/lib/unittest/lib/mocha/mocha.js:5989:27)"
		      }
		    },
		    {
		      "title": "3. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 3. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "4. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 4. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 1001,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    }
		  ],
		  "pending": [
		    {
		      "title": "3. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 3. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "4. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 4. should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 3. #indexOf() should return -1 when the value is not present",
		      "err": {}
		    }
		  ],
		  "failures": [
		  ],
		  "passes": [
		    {
		      "title": "1. should return -1 when the value is not present",
		      "fullTitle": "Array 1. #indexOf() 1. should return -1 when the value is not present",
		      "duration": 1,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 0,
		      "err": {}
		    },
		    {
		      "title": "should return -1 when the value is not present",
		      "fullTitle": "Array 2. #indexOf() should return -1 when the value is not present",
		      "duration": 1001,
		      "err": {}
		    }
		  ]
		}
	}
});


function waitOver(){
	try{
		return mocha.runner.code == mocha.runner.CODE_ENDED;
	}catch(e){
		// Force return false to timeout.
		return false;
	}
}

function waitResult(){
	try{
		//throw new Error("sds");
		var testResults = mocha.runner.testResults;
		return JSON.stringify(testResults, null, 2)
	}catch(e){
		var msg = "Error: " + e.message;
		return JSON.stringify(msg, null, 2);
	}
}

function pageSource(src){
	return src.replace(/<link.*?mocha.css.*? \/>/g, "<link href=\"http://cdn.bootcss.com/mocha/2.0.1/mocha.css\" rel=\"stylesheet\">");
}


exports.testConf = testConf;
exports.caseList = caseList;
