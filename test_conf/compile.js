"use strict";

/**
 * 从json文件中读取caseList和testConf，编译summary报告
 */

var fs = require("fs");

var reportCompiler = require("../lib/reportCompiler.js");

var caseList = null;
var testConf = null;

var caseListContent = fs.readFileSync(__dirname + "/caseList.json");
caseList = JSON.parse(caseListContent);

var testConfContent = fs.readFileSync(__dirname + "/testConf.json");
testConf = JSON.parse(testConfContent);


exports.caseList = caseList;
exports.testConf = testConf;

reportCompiler.compileSummaryReport(caseList, testConf);



