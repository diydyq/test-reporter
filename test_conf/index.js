"use strict";

/**
 * 新建caseList，执行测试，生成测试报告
 */

var caseList = [];
caseList.push({
    name: "lib:unittest/cases/mocha/case.vm",
    url: "/lib/unittest/cases/mocha/case.vm"
});
//caseList.push({
//    name: "lib:unittest/cases/mocha/case.vm",
//    url: "localhost:80/lib/unittest/cases/mocha/case.vm"
//});
//caseList.push({
//    name: "lib:unittest/cases/mocha/case.vm",
//    url: "http://localhost:80/lib/unittest/cases/mocha/case.vm"
//});
//caseList.push({
//    name: "common:unittest/cases/TreeNodesSelect/test.vm",
//    url: "/common/unittest/cases/TreeNodesSelect/test.vm"
//});

/*
// Example.
caseList.push({
    name: "common:unittest/cases/TreeNodesSelect/test.vm",
    url:  "/common/unittest/cases/TreeNodesSelect/test.vm",
    http: "http://host:port/XXX",                   // Full requested URL.
    hash: {
        chrome: {
             result: {
                 flag: true,    	                        // false: timeout; true: execute succeed.
                 code: 0,				                    // 0: ready; 1: running; 2: ended.
                 text: "",				                    // JSON string of case results.
                 json: {},				                    // JSON object parsed from string.
                 page: {
                     name: "",			                    // prefix of case name.
                     html: "",			                    // whole html text.
                     pure: ""			                    // only mocha html.
                 }
             }
        }
    }
});

*/


exports.caseList = caseList;


//var runner = require("../index");
var runner = require("./saveJson");
runner.runTest(caseList);


