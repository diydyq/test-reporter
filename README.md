test-reporter
============
test-reporter is a web test runner and report generator. 

How to use it and process:

1. User define test configuration object and test cases.

2. Program executes the test cases by using selenium.

3. After execution, it generates summary report.

What is used in project?

1. Mocha:

Compared with QUnit and Jasmine, Mocha presents more advantages: support more assert js libraries, multiple report style, active user groups.

But there is also one issue while using it, Mocha does not support multiple report at the same time, so I extend Mocha.Runner for this feature.

2. selenium-webdirver

Uses selenium webdriver to control browser.
