"use strict";

var modMain = angular.module("reportServer", [
    "ui.bootstrap",
    "ngRoute",
    "ngAnimate",


    "reportController",
    "reportService"
]);

/**
 * Work with: $route, $location.
 * Steps for $routeProvider:
 * 1. Match URLs.
 * 2. Extract params and put into $routeParams.
 * 3. New controller, and inject $routeParams, $location if necessary.
 * 4. Load template, and render
 */
modMain.config(["$routeProvider", function($routeProvider){
    $routeProvider
        .when("/reportList", {
            templateUrl: "views/reportList.html",
            controller: "reportList"
        })
        .when("/reportList/:caseName", {
            templateUrl: "views/reportCase.html",
            controller: "reportCase",
            resolve: {
                caseParams: function($q, $timeout){
                    var def = $q.defer();
                    $timeout(function(){
                        def.resolve({name: "custom param value"});
                    }, 0);
                    return def.promise;
                }
            }
        })
        .when("/pagination/test", {
            templateUrl: "views/paginationTest.html",
            controller: "PaginationTestController"
        })
        .otherwise({
            redirectTo: "/reportList"
        });
}]);



var modController = angular.module("reportController", []);
modController.controller("HomeController", ["$scope", "$modal", "$log", function($scope, $modal, $log){
    // alert.
    $scope.showbox = null;
    $scope.alert = function(msg, icon){
        $scope.showbox = {
            msg: msg,
            type: icon
        };
    };
    $scope.closebox = function(){
        console.log(arguments);
    };

    // Collapse.
    $scope.closeJumbotron = false;

    // Dialog.
    $scope.showDialog = function(size, scope){
        var inst = $modal.open({
            templateUrl:    "views/customDialog.html",
            controller:     "ModalController",
            scope:          scope,
            size:           size,
            resolve:        {
                // Resolved as a service.
                resolvedScope:  function(){
                    return $scope;
                }
            }
        });

        inst.result.then(function(reason){
            $log.log("Submit:", reason);
        }, function(reason){
            $log.log("Cancel:", reason);
        });
    };

}]);
/**
 * 不要设置名为：PaginationController，因为ui.bootstrap中已经定义该controller
 */
modController.controller("PaginationTestController", ["$scope", "$log", "SvcPagination", function($scope, $log, SvcPagination){
    $scope.beanList = SvcPagination.getPageList();

    $scope

}]);
/**
 * Modal对话框
 */
modController.controller("ModalController", ["$scope", "$modalInstance", "resolvedScope", function($scope, $modalInstance, resolvedScope){
    $scope.username = "";
    $scope.resolvedScope = resolvedScope;
    $scope.btnSubmit = function(){
        $modalInstance.close($scope.username);
    };
    $scope.btnCancel = function(){
        $modalInstance.dismiss({
            reason:     "Cancel",
            opener:     resolvedScope
        });
    };
}]);

modController.controller("reportList", ["$scope", "$q", "$timeout", "$filter", "$http", "SvcFileRead", "SvcTestUtil", function($scope, $q, $timeout, $filter, $http, SvcFileRead, SvcTestUtil){
    $scope.test = "welcome!";
    $scope.caseFilter = "";
    $scope.caseList = SvcFileRead.getCaseList();
    $scope.testConf = SvcFileRead.getTestConf();
    $scope.testList = SvcTestUtil.getCaseList($scope);

   /**
     * 根据$scope.caseFilter来过滤
     */
    $scope.onCaseFilterChange = function(){
        $scope.cFilteredList2 = $filter("filter")($scope.testList, function(c, i){
            for(var cKey in c){
                if(typeof c[cKey] == "string" && c[cKey].indexOf($scope.caseFilter) != -1){
                    return true;
                }
            }
        });
        $scope.alert("This is message", "success");
    };

    // Clear contents after hidden.
    $("#modal-case").on("hidden.bs.modal", function (e) {
        $("#modal-case").find(".modal-title").empty();
        $("#modal-case").find(".modal-body").empty();
    });

    $scope.openReportDialog = function(c, index, cPage){
        $("#modal-case").find(".modal-title").append(c.name);
        $(cPage.pure).appendTo($("#modal-case").find(".modal-body"));
    };

    $scope.openSnapshotDialog = function(c, index, cPage){
        $("#modal-case").find(".modal-title").append(c.name);
        var srcData = "data:image/png;base64, " + cPage.shot;
        $("<img>").prop("src", srcData).appendTo($("#modal-case").find(".modal-body"));
    };

    $scope.showCustomDialog = function(){
        $scope.title = $scope.testConf.summary.fileName;
        $scope.showDialog("", this);
    };
}]);

modController.controller("reportCase", ["$scope", "$route", "$routeParams", "caseParams", function($scope, $route, $routeParams, caseParams){
    $scope.params = $routeParams;
    $scope.$route = $route;
    $scope.caseParams = caseParams;
}]);



var modService = angular.module("reportService", ["ngResource"]);
modService.factory("SvcFileRead", ["$resource", function($resource){
    return $resource("serverData/:file.json", {}, {
        getCaseList: {
            method: "GET",
            params: {
                file: "caseList"
            },
            isArray: true
        },
        getTestConf: {
            method: "GET",
            params: {
                file: "testConf"
            }
        }
    });
}]);

modService.factory("SvcTestUtil", ["$q", "SvcFileRead", function($q, SvcFileRead){
    return {
        /**
         * 获取所有case，包括各浏览器版本.
         * @param $scope
         * @returns {Array}
         */
        getCaseList: function($scope){
            var self = this;
            var def = $q.all([$scope.caseList.$promise, $scope.testConf.$promise]).then(function(defDataList) {
                var list = self.getTestListByConf(defDataList[0], defDataList[1]);

                // 填充数据
                list.forEach(function(v){
                    ret.push(v);
                });
                ret.$resolved = true;

                // 其他的业务逻辑
                var summary = $scope.testConf.summary;
                summary.timeStartText = moment(summary.timeStart).format("YYYY-MM-DD HH:mm:ss");
                summary.timeEndText = moment(summary.timeEnd).format("YYYY-MM-DD HH:mm:ss");
                summary.durationText = (moment(summary.timeEnd) - moment(summary.timeStart))/1000.0 + " seconds";

                return ret;
            });

            // 返回针对资源的封装：$promise
            var ret = [];
            ret.$promise = def;
            ret.$resolved = false;
            return ret;
        },
        /**
         * 获取所有case，包括各浏览器版本.
         * @param caseList 所有cases.
         * @param testConf 配置conf
         * @returns {Array}
         */
        getTestListByConf: function(caseList, testConf){
            var testList = [];
            testConf.browsers.forEach(function(vB, i){
                caseList.forEach(function(vC, i){
                    var cHash = vC.hash[vB];
                    cHash.name = vC.name;
                    cHash.url  = vC.url;
                    cHash.http = vC.http;

                    testList.push(cHash);
                });
            });

            return testList;
        }
    };
}]);

modService.factory("SvcPagination", [function(){
    return {
        getPageList: function(){
            var beanList = [];
            for(var i=0; i<2; i++){
                var bean = {
                    id:     i,
                    name:   "Bean"+(i+1)
                };
                beanList.push(bean);
            }

            return {
                pageSize:     10,
                data:         beanList,
                currentPage:  5,
                totalPage:    17,
                totalCount:   200,
                pageChanged:  function() {
                    console.log("Page changed to: ", this.currentPage);
                }
            };
        }
    };
}]);
modService.constant("paginationConfig", {
    itemsPerPage:   10,
    boundaryLinks:  true,
    directionLinks: true,
    previousText:   "上一页",
    nextText:       "下一页",
    firstText:      "首页",
    lastText:       "尾页",
    rotate:         true,
    // maxSize: 页面中显示多少个页签
    maxSize:        9
});

