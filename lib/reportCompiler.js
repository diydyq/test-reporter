"use strict";


var fs = require("fs");
var jade = require('jade');
var moment = require("moment");

/**
 * Compile jade template to generate summary report HTML.
 * @param caseList
 * @param testConf
 */
function compileSummaryReport(caseList, testConf){
	if(!caseList){
		// TODO: remove test data.
		var testData = require("../test/test_data1.js");
		caseList = testData.caseList;
		testConf = testData.testConf;
	}

	var jadeObj = {
		list: caseList,
		conf: testConf,
        moment: moment
	};

    var fn = jade.compileFile(__dirname + "/reportSummaryTmpl.jade", {
		pretty: true
	});
	var html = testConf.summary.fileHtml = fn(jadeObj);

	//console.log(html);
	//fs.writeFileSync(jadeObj.conf.summary.fileName, html);
}

exports.compileSummaryReport = compileSummaryReport;
