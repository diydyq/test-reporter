"use strict";

var fs = require("fs");
var webdriver = require("selenium-webdriver");
var firefox = require("selenium-webdriver/firefox");

var cfg = {
};

/**
 * Configure.
 */
function setup(){
    if(process.platform == "win32"){
        var envPath = process.env.path;

        var exePath = __dirname + "/../thirdparty";
        process.env.path = (process.env.path + ";" + exePath);
        console.info("Path add driver: " + exePath);
    }
}

/**
 * Running test cases and generate one Deferred, when running over, def was resolved.
 * @param caseList
 * @param testConf
 * @returns {run}
 */
function run(caseList, testConf){
    setup();

    exports.def = new webdriver.promise.Deferred();

	cfg = testConf;
    cfg.domain = getDomain(cfg);

    cfg.summary.timeStart = new Date();

    cfg.browsers.forEach(function(v, i){
        runInDriver(v, caseList);
	});

    return exports;
}

/**
 * Load driver by specific browser.
 * @param browserName
 * @returns {*}
 */
function getDriver(browserName){
    var driver = null;
    if(/chrome/i.test(browserName)){
        driver = (new webdriver.Builder()).withCapabilities(webdriver.Capabilities.chrome()).build();
    }else if(/firefox/i.test(browserName)){
        driver = new firefox.Driver();
    }else{
        console.error("Not supported browser: " + browserName);
    }
    return driver;
}

function runInDriver(browserName, caseList){
	caseList.forEach(function(v, i){
		var caseObj = v;

		var caseURL = caseObj.http = getRequetURL(cfg, caseObj.url);

        // New driver for each case.
        var driver = getDriver(browserName);
        if(driver){
            driver.get(caseURL);
            console.log("Test URL : ", caseURL);

            // Step1. Wait for case ended.
            var def1 = driver.wait(function() {
                return driver.executeScript(cfg.testLib.fnCaseIsEnded);
            }, cfg.caseTimeout, "Timeout to wait case end.");

            // Step2. Get case result.
            def1.then(function(boolDone){
                console.log("Test Done: ", caseURL, boolDone);
                getResult(browserName, driver, caseObj, true, caseList);
            }, function(error){
                console.warn("Test Time: ", caseURL);
                getResult(browserName, driver, caseObj, false, caseList);
            });
        }
	});
}

function getResult(browserName, driver, caseObj, isDone, caseList){
    driver.executeScript(cfg.testLib.fnCaseResult).then(function(resultText){

        // hash.browserName
        caseObj.hash = caseObj.hash || {};
        caseObj.hash[browserName] = {};
        caseObj.hash[browserName].browserName = browserName;

        // result: flag, text, json, code
        var caseResult = caseObj.hash[browserName].result = {};

        caseResult.flag = isDone;
        caseResult.text = resultText;
        caseResult.json = JSON.parse(resultText);
        caseResult.code = caseResult.json.code;

        // page: name, html, pure
        var page = caseResult.page = {};
        page.name = caseObj.url.replace(/[\:\/\\\.]+/g, ".");

        // page source.
        driver.getPageSource().then(function(src){
            page.html = cfg.testLib.fnCaseSource ? cfg.testLib.fnCaseSource(src) : src;
            driver.findElement(webdriver.By.id("mocha")).then(function(webElement){
                webElement.getOuterHtml().then(function(src){
                    page.pure = src;
                });
            }, function(error){
                console.error("Test WARN: ", "Could not get div#mocha: " + error.message);
                page.pure = "";
            })
            .thenFinally(function(){
                // Take a screen shot.
                driver.takeScreenshot().then(function(data){
                    page.shot = data;

                    //
                    if(!isDone){
                    }else{
                    }

                    // Quit.
                    driver.quit();

                    if(caseObj == caseList[caseList.length-1]){
                        // Resolve this deferred after ended.
                        if(cfg.browsers.indexOf(browserName) == cfg.browsers.length-1){
                            cfg.summary.timeEnd = new Date();
                            exports.def.fulfill(caseList);
                        }
                    }
                });
            });
        });
    });
}


/**
 * Guess the domain of URL. eg: "http://localhost:8080/".
 * @param {object} cfg Configuration of "protocal", "host", "port".
 * @returns {string}
 */
function getDomain(cfg){
	var domain = "";

	if(cfg.protocal){
		domain += (cfg.protocal + "://");
	}

	domain += (cfg.host);

	if(cfg.port){
		domain += (":" + cfg.port);
	}

	return domain;
}

/**
 * Add protocal, host, port for URL.
 */
function getRequetURL(cfg, url){

    if(/^http.*/.test(url)){
        // URL starts with protocal.
        return url;
    }else if(/^\/.*/.test(url)){
        // URL does not contain host, eg: /test/index.html
        return getDomain(cfg) + url;
    }else{
        // URL contains domain. eg: www.google.com
        var prefix = (cfg.protocal + "://");
        return prefix + url;
    }
}

exports.run = run;

/**
 * Deferred was resolved when all cases were executed.
 * @type {webdriver.promise.Deferred}
 */
exports.def = null;



