"use strict";

var fs = require("fs");
var moment = require("moment");

/**
 * Generate report at specified location.
 * @param caseList
 * @param testConf
 */
function generateReports(caseList, testConf){

    setReportLocation(testConf);

	var serverPath = testConf.serverConf.root;
	var summary = testConf.summary;

    // Summary directory name by date+time.
	var dirName = moment(summary.timeStart).format("YYYYMMDDHHmmss");
    summary.reportDirName = dirName;

	var dirPath = serverPath + dirName + "/";
	fs.mkdirSync(dirPath);
	console.log("Report Root: " + dirPath);

	// Summary report.
	var summaryPath = dirPath + testConf.summary.fileName;
	fs.writeFileSync(summaryPath, summary.fileHtml);
	console.log("Report path: " + summaryPath);

	// Case Report: html and snapshot.
    testConf.browsers.forEach(function(browserName, j){
        caseList.forEach(function(tCase, i){
            var tBrow = tCase.hash[browserName];
            var tRsut = tBrow.result;
            var tName = dirPath + browserName + tRsut.page.name;

            var htmlName = tName + summary.htmlType;
            fs.writeFileSync(htmlName, tRsut.page.html);
            console.log("Report case HTML: " + htmlName);

            var shotName = tName + summary.shotType;
            var base64Data = tRsut.page.shot.replace(/^data:image\/png;base64,/, "");
            fs.writeFileSync(shotName, base64Data, "base64");
            console.log("Report case SHOT: " + shotName);
        });
    });
}

/**
 * Create directory by serverConf.root or serverConf.name
 * @param conf
 */
function setReportLocation(conf){
    var sConf = conf.serverConf;

    if(!sConf.root){
        var list = ["FIS_TEMP_DIR", "LOCALAPPDATA", "APPDATA", "HOME"];
        var tmp;
        for(var i = 0, len = list.length; i < len; i++){
            if(tmp = process.env[list[i]]){
                break;
            }
        }
        tmp = tmp || __dirname + "/../";
        sConf.root = tmp + "/" + sConf.name + "/";
    }

    if(!fs.existsSync(sConf.root)){
        fs.mkdirSync(sConf.root);
    }

    console.log("Report server: ", sConf.root);
}

exports.generateReports = generateReports;
