"use strict";

/*
 var testConf = {
     browsers: ["chrome", "firefox"],
     protocal: "http",
     host: "localhost",
     port: null,
     caseTimeout: 10*1000,
     testLib: {
         name: "mocha",
         fnWaitFlag: waitOver,
         fnWaitReturn: waitResult,
         fnPageSource: pageSource
     },

     summary: {
         fileName: "report_summary.html",		// file name.
         fileHtml: ""							// HTML text.
         timeStart: new Date,
         timeEnd: new Date,
         reportDirName: "",

         htmlType: ".html",                     // caseName.html
         shotType: ".png",                      // caseScreenshot.png
         open: true

     },
     serverConf: {
        name: ".tmpServer"
     }
 };



 */


var defaultConf = {
    browsers: ["chrome", "firefox"],
    protocal: "http",
    host: "localhost",
    port: null,
    caseTimeout: 10*1000,
    testLib: {
        name: "mocha",
        fnCaseIsEnded: caseIsEnded,
        fnCaseResult:  caseResult,
        fnCaseSource:  caseSource
    },

    summary: {
        fileName: "report_summary.html",
        htmlType: ".html",
        shotType: ".png",
        open: true
    },
    serverConf: {
        name: ".tmpTestReporterServer"
    }
};

/**
 * Executed in browser to check if test ended.
 * If return false, then this function will be executed next time util true or timeout.
 * @returns {boolean}
 */
function caseIsEnded(){
    try{
        return mocha.runner.code == mocha.runner.CODE_ENDED;
    }catch(e){
        // Force return false to turn to timeout.
        return false;
    }
}

/**
 * Executed in browser to get testResults.
 * @returns {*}
 */
function caseResult(){
    try{
        //throw new Error("Simulated error message.");
        var testResults = mocha.runner.testResults;
        return JSON.stringify(testResults, null, 2);
    }catch(e){
        var obj = {
            "msg": "Error: " + e.message
        };
        return JSON.stringify(obj, null, 2);
    }
}

/**
 * HTML text of the test case. Usually filter some tag.
 * @param {string} HTML text returned by driver.getPageSource();
 * @returns {*|XML|string|void} Filtered text that will be cased in each case object.
 */
function caseSource(htmlSource){
    return htmlSource.replace(/<link.*?mocha.css.*? \/>/g, "<link href=\"http://cdn.bootcss.com/mocha/2.0.1/mocha.css\" rel=\"stylesheet\">");
}

exports.defaultConf = defaultConf;


