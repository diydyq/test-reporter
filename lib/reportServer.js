"use strict";

var _ = require("underscore");
var fs = require("fs");
var net = require("net");
var httpServer = require("http-server");
var opener = require("opener");

var defaultConf = {
    name: ".tmpTestReporterServer",
	port: 8000,
	host: "localhost",
    // context path.
    root: null
};

function startHttpServer(testConf){
    var options = testConf.serverConf;
    if(!options.root){
        options.root = options.name
    }
    if(!fs.existsSync(options.root)){
        fs.mkdirSync(options.root);
    }

    setServerURL(testConf);

	var server = httpServer.createServer(options);
	server.listen(options.port, options.host, function() {
		console.log("Server started at:", options.port);
	});

	if (process.platform !== 'win32') {
		process.on('SIGINT', function () {
			console.log('Server stopped.');
	    	process.exit();
		});
	}
}

/**
 * Set server domain URL.
 * @param testConf
 */
function setServerURL(testConf){
    var sConf = testConf.serverConf;
    sConf.url = "http://$host:$port/".replace("$host", sConf.host).replace("$port", sConf.port);

    var fullURL = testConf.serverConf.url + testConf.summary.reportDirName + "/" + testConf.summary.fileName;
    console.log("Report URL : ", fullURL);

    // Open in browser.
    if(testConf.summary.open){
        opener(fullURL);
    }
}

/**
 * Start server if the port is not used.
 * @param testConf
 */
function startServer(testConf){
    testConf.serverConf = _.extend(defaultConf, testConf.serverConf);

	// Test if port was used.
	var portServer = net.createServer();
    portServer.listen(defaultConf.port, function(){
        // Not used: close and then start HTTP server.
        portServer.close(function(){
            startHttpServer(testConf);
        });
    });
    portServer.on("error", function (e) {
        if (e.code == "EADDRINUSE") {
            // Server has started.
            console.warn("Server had been listening at port: " + defaultConf.port);
            setServerURL(options);
        }else{
            // Other listen errors.
            console.error("Server listens port[$0] error: ".replace("$0", defaultConf.port), e);
        }
    });

}

exports.startServer = startServer;

